import threading
import pygame
from pygame.locals import *
import os
import time

WHITE = (255,255,255)

class PiTFT_Controller(threading.Thread):

    def __init__(self,px_width=320,px_height=240,FPS_lock=20,MOUSE_VISIBLE=False,DB_SESSION_FACTORY=None):
        threading.Thread.__init__(self)

        os.putenv('SDL_FBDEV', '/dev/fb1')
        os.putenv('SDL_MOUSEDRV', 'TSLIB')
        os.putenv('SDL_MOUSEDEV', '/dev/input/touchscreen')

        self.daemon = True
        self.running = False

        self.__width = px_width
        self.__height = px_height
        self.fps_lock = FPS_lock
        self.__start_loop = time.time()
        self.__event_filter = _event_filter(DB_SESSION_FACTORY=DB_SESSION_FACTORY)
        self.lcd = None
        self.change = True
        self._frames_since_update = 0

        pygame.init()
        self.lcd = pygame.display.set_mode((px_width, px_height), 0, 32)
        pygame.mouse.set_visible(MOUSE_VISIBLE)
        pygame.display.update()
        time.sleep(0.5)

        self.active_screen = "MAIN"
        self.curr_ui_objects = {self.active_screen:[]}

        if DB_SESSION_FACTORY:
            self.db_session = DB_SESSION_FACTORY()

        print("PITFT init done")


    def run(self):
        self.running = True
        self._start()

    def _start(self):

        self.__event_filter.start()

        while self.running:
            self.__start_loop = time.time()
            self.check_clicks()
            if self.change or self._frames_since_update > 10:
                self.lcd.fill([0, 0, 0])
                self.update_ui_objects()
                self.change =False
                self._frames_since_update = 0

            ###############################################
            #   Enter function calls here



                pygame.display.update()

            elapsed_time = time.time() - self.__start_loop
            frame_time = 1.0 / self.fps_lock
            delay_time  = frame_time - elapsed_time
            if delay_time > 0 and delay_time < 1:
                #print("Sleeping %.3f seconds"%delay_time)
                time.sleep(delay_time)

            self._frames_since_update += 1







    def stop(self):
        self.running = False
        if self.__event_filter.running:
            self.__event_filter.running = False

    def __del__(self):
        self.stop()

    #   Function to check all objects on screen then check if clicked
    def check_clicks(self):
        if self.__event_filter.to_be_unclicked:
            self.change = True
            self.__event_filter.to_be_unclicked = False


        while len(self.__event_filter.clicks) > 0:
            #curr_mouse_click = self.__event_filter.clicks[0]
            curr_mouse_click = self.__event_filter.clicks.pop(0)
            for ui_object in self.curr_ui_objects[self.active_screen]:
                if ui_object.check_pos(curr_mouse_click):
                    self.change = True

    def add_button(self, X1, Y1, X2, Y2, TEXT="", CALLBACK=None, SHAPE='RECT', HIDDEN=False, THICK=2,
                       COLOR=Color("red"), FILL=0.0, SCREEN="MAIN"):

        new_button = button(self.lcd, X1, Y1, X2, Y2, SHAPE=SHAPE, HIDDEN=HIDDEN, THICK=THICK, COLOR=COLOR, TEXT=TEXT,
                            FILL=FILL, CLICK_CALLBACK=CALLBACK)
        if not SCREEN in self.curr_ui_objects.keys():
            self.curr_ui_objects[SCREEN] = []
        self.curr_ui_objects[SCREEN].append(new_button)
        self.change = True
        return new_button

    def add_screen_button(self, X1, Y1, X2, Y2, SCREEN_JMP="OTHER_OPTS", TEXT="", CALLBACK=None, SHAPE='RECT',
                              HIDDEN=False, THICK=2,
                              COLOR=Color("red"), FILL=0.0, SCREEN="MAIN"):


        new_button = change_screen_button(self, self.lcd, X1, Y1, X2, Y2, SHAPE=SHAPE, HIDDEN=HIDDEN, THICK=THICK, COLOR=COLOR,
                                          TEXT=TEXT,
                                          FILL=FILL, SCREEN_JMP=SCREEN_JMP, CLICK_CALLBACK=CALLBACK)
        if not SCREEN in self.curr_ui_objects.keys():
            self.curr_ui_objects[SCREEN] = []
        self.curr_ui_objects[SCREEN].append(new_button)
        self.change = True
        return new_button

    def add_screen_table(self, X1, Y1, X2, Y2,COLOR=Color("red"),SCREEN="MAIN"):
        new_screen_table = screen_table(self.lcd,X1,Y1,X2,Y2, THICK=2, COLOR=Color("red"), FONT=None)

        if not SCREEN in self.curr_ui_objects.keys():
            self.curr_ui_objects[SCREEN] = []
        self.curr_ui_objects[SCREEN].append(new_screen_table)
        self.change = True


        return new_screen_table

    def update_ui_objects(self):
        for x in range(len(self.curr_ui_objects[self.active_screen])):
        #for ui_obj in self.ui_objects:
            if not self.curr_ui_objects[self.active_screen][x].hidden:
                self.curr_ui_objects[self.active_screen][x].draw()
        pass



class _event_filter(threading.Thread):
    def __init__(self,SLEEP_TIME=0.025,DB_SESSION_FACTORY=None):
        threading.Thread.__init__(self)

        self.daemon = True
        self.running = False
        self.sleep_time = SLEEP_TIME

        self.clicks = []
        self.to_be_unclicked = False
        self.event_list = [pygame.MOUSEBUTTONUP,pygame.MOUSEBUTTONDOWN]

        if DB_SESSION_FACTORY:
            self.db_session = DB_SESSION_FACTORY()

        print("event handler init done")

    def run(self):
        self.running = True
        self._start()

    def _start(self):
        while self.running:
            event_cnt = 0
            for e in pygame.event.get():
                mousepoint = pygame.mouse.get_pos()
                if not self.running:
                            break
                if e.type == pygame.MOUSEBUTTONDOWN:
                    #print("Down",mousepoint)
                    self.clicks.append(mousepoint)
                    event_cnt += 1
                elif e.type == pygame.MOUSEBUTTONUP:
                    #print("UP",mousepoint)
                    self.to_be_unclicked = True
                    event_cnt +=1

            time.sleep(self.sleep_time)




    def stop(self):
        self.running = False
        pygame.event.clear()

    def __del__(self):
        self.stop()


#   Object to handle a GUI objects status of clicks and actions
class _ui_object():
    X_low = 0
    X_high = 0
    Y_low = 0
    Y_high = 0
    def __init__(self,X1,Y1,X2,Y2):

        if X1 < X2:
            self.X_low = X1
            self.X_high = X2
        else:
            self.X_low = X2
            self.X_high = X1

        if Y1 < Y2:
            self.Y_low = Y1
            self.Y_high = Y2
        else:
            self.Y_low = Y2
            self.Y_high = Y1

        print("UI object init done")

    #   Function to check if mouse is over this object
    #   Simplified boxing method
    def check_pos(self,XY_LIST):

        x = XY_LIST[0]
        y = XY_LIST[1]
        #print("X1: %d,Y1: %d,X2: %d,Y2:%d\tX:%d Y:%d" % (self.X_low,self.Y_low,self.X_high,self.Y_high,x,y))
        if y >= self.Y_low and y <= self.Y_high:
            if x >= self.X_low and x <= self.X_high:
                return True
        return False


class button(_ui_object):
    def __init__(self,SURFACE,X1,Y1,X2,Y2,SHAPE='RECT',HIDDEN=False,THICK=2,COLOR=Color("red"),
                COLOR_ON=Color("green"),CLICK_CALLBACK=None,TEXT="",FONT=None,FILL=0.0):
        self.hidden = True
        super().__init__(X1,Y1,X2,Y2)

        if SHAPE.upper() is'ELIPSE':
            self.__mode = "ELIPSE"
        else:
            self.__mode = "RECT"


        self.surface = SURFACE
        self.thickness = THICK
        self.err_color = Color("blue")

        self.norm_color = COLOR
        self.norm_color_bkup = COLOR
        self.on_click_color = COLOR_ON
        self.__color = COLOR
        self.fill = FILL
        if FONT:
            self.font = FONT
        else:
            self.font = pygame.font.Font(None, 30)

        self.text = TEXT

        self.text_surface = self.font.render(self.text, True, WHITE)


        self.CALLBACK = CLICK_CALLBACK
        self.clicked = False
        self.clicked_toggle = False

        self.hidden = HIDDEN

    def draw(self):
        if not self.hidden:

            if self.__mode == "ELIPSE":
                pygame.draw.ellipse(self.surface, self.norm_color,
                                    [self.X_low, self.Y_low, self.X_high - self.X_low, self.Y_high - self.Y_low],0)
            else:
                pygame.draw.rect(self.surface, self.norm_color,
                                 [self.X_low, self.Y_low, (self.X_high - self.X_low),
                                  (self.Y_high - self.Y_low)],0)

            if self.clicked:
                if self.__mode == "ELIPSE":
                    pygame.draw.ellipse(self.surface, self.__color,
                                        [self.X_low, self.Y_low, self.X_high - self.X_low, self.Y_high - self.Y_low],
                                        self.thickness)
                else:
                    pygame.draw.rect(self.surface, self.__color,
                                     [self.X_low, self.Y_low, self.X_high - self.X_low, self.Y_high - self.Y_low],
                                     self.thickness)


            if self.fill > 0 and self.fill <= 1:
                if self.__mode == "ELIPSE":
                    # pygame.draw.ellipse(self.surface, self.on_click_color,
                    #                     [self.X_low, self.Y_low, self.X_high - self.X_low, self.Y_high - self.Y_low],
                    #                     self.thickness)
                    pass
                else:
                    pygame.draw.rect(self.surface, self.on_click_color,
                                     [self.X_low, self.Y_low, (self.X_high - self.X_low), ((self.Y_high - self.Y_low)* self.fill)],0)

            else:
                if self.__mode == "ELIPSE":
                    pygame.draw.ellipse(self.surface, self.__color, [self.X_low, self.Y_low, self.X_high-self.X_low, self.Y_high-self.Y_low], self.thickness)
                else:
                    pygame.draw.rect(self.surface, self.__color, [self.X_low, self.Y_low, self.X_high-self.X_low, self.Y_high-self.Y_low], self.thickness)

            if self.text:
                self.text_surface = self.font.render(self.text, True, WHITE)
                self.surface.blit(self.text_surface, self._calc_text_location())
            self.__color = self.norm_color

    def _calc_text_location(self):
        #self.text_surface = self.font.render(self.text, True, WHITE)
        text_x = self.text_surface.get_width()
        text_y = self.text_surface.get_height()

        button_width = self.X_high - self.X_low
        button_heigth = self.Y_high - self.Y_low

        if text_x < button_width:
            calc_x = ((button_width - text_x) / 2) + self.X_low
        else:
            calc_x = self.X_low

        if text_y < button_heigth:
            calc_y = ((button_heigth - text_y) / 2) + self.Y_low
        else:
            calc_y = self.Y_low

        return (calc_x,calc_y)



    def check_pos(self,XY_LIST):
        x = XY_LIST[0]
        y = XY_LIST[1]
        #print("BUTTON",self.X_low,self.Y_low,self.X_high,self.Y_high,XY_LIST)

        if super().check_pos(XY_LIST):

            self.clicked = True
            self.__color = self.on_click_color

            if self.clicked_toggle:
                self.clicked_toggle = False
            else:
                self.clicked_toggle = True
            if self.CALLBACK:
                self.CALLBACK(SELF_REF=self)
            return True

        else:
            self.clicked = False
            self.__color = self.norm_color
            return True

class change_screen_button(button):
    #   First click gives a full took amount, proceeding ones will drop the dosage
    def button_click_callback(self, SELF_REF=None):
        self.PiTFT_link.active_screen = self.screen_jump_to
        self.PiTFT_link.change = True

        if self.optional_callback:
            if self.optional_callback(SELF_REF=self):
                self.PiTFT_link.active_screen = self.screen_jump_to
                self.PiTFT_link.change = True
        else:
            self.PiTFT_link.active_screen = self.screen_jump_to
            self.PiTFT_link.change = True

    def __init__(self,PITFT_CLASS,SURFACE,X1,Y1,X2,Y2,SHAPE='RECT',HIDDEN=False,THICK=2,COLOR=Color("red"),
                COLOR_ON=Color("green"),TEXT="",FONT=None,FILL=0.0,SCREEN_JMP="OTHER_OPTS",CLICK_CALLBACK=None):

        super().__init__(SURFACE,X1,Y1,X2,Y2,SHAPE='RECT',HIDDEN=False,THICK=2,COLOR=Color("red"),
                COLOR_ON=Color("green"),CLICK_CALLBACK=self.button_click_callback,TEXT=TEXT,FONT=None,FILL=0.0)

        self.screen_jump_to = SCREEN_JMP
        self.PiTFT_link = PITFT_CLASS
        self.optional_callback = CLICK_CALLBACK
        pass


class screen_table(_ui_object):
    def __init__(self, SURFACE, X1, Y1, X2, Y2, HIDDEN=False, THICK=2, COLOR=Color("red"), FONT=None,MARGIN=1):


        self.hidden = True
        super().__init__(X1, Y1, X2, Y2)

        self.__mode = "RECT"

        self.surface = SURFACE
        self.thickness = THICK
        self.norm_color = COLOR

        if FONT:
            self.font = FONT
        else:
            self.font = pygame.font.Font(None, 30)
        self.text_arr  = [["NO"," DATA"," YET"],["NO"," DATA"," YET"]]
        self.text_surface = None
        self.margin = MARGIN


        self.hidden = HIDDEN


    def draw(self):
        if not self.hidden:
            coll_widths = [0]
            coll_heights = [0]

            text_width = 0
            text_height = 0
            for row_idx in range(len(self.text_arr)):
                if row_idx >= len(coll_heights):
                    coll_heights.append(0)


                for cell_idx in range(len(self.text_arr[row_idx])):
                    if cell_idx >= len(coll_widths):
                        coll_widths.append(0)

                    cell = self.text_arr[row_idx][cell_idx]
                    if cell:
                        text_surface = self.font.render(str(cell), True, WHITE)
                        text_width = text_surface.get_width()
                        text_height = text_surface.get_height()

                    if text_width >= coll_widths[cell_idx]:
                        coll_widths[cell_idx] = text_width

                if text_height >= coll_heights[row_idx]:
                    coll_heights[row_idx] = text_height

            ORIGIN_Y = self.margin
            for row_idx in range(len(self.text_arr)):
                ORIGIN_X = self.margin
                for cell_idx in range(len(self.text_arr[row_idx])):

                    cell = self.text_arr[row_idx][cell_idx]
                    text_surface = self.font.render(str(cell), True, WHITE)

                    if (ORIGIN_X + text_surface.get_width()) <= self.X_high and (ORIGIN_Y + text_surface.get_height()) <= self.Y_high:

                        self.surface.blit(text_surface, (ORIGIN_X,ORIGIN_Y))
                    ORIGIN_X += (coll_widths[cell_idx] + self.margin)

                ORIGIN_Y += (coll_heights[row_idx] + self.margin)

