import sys
from pygame_thread_ui.ui_tools import *



if __name__ == "__main__":
    test_PiTFT = None
    try:
            test_PiTFT = PiTFT_Controller(MOUSE_VISIBLE=False)
            test_PiTFT.start()

            # Row1
            RowY = 20
            RowBottom = RowY + 60
            toook_eoin = test_PiTFT.add_button(10, RowY, 100, RowBottom, TEXT="EOIN")
            toook_james = test_PiTFT.add_button(110, RowY, 210, RowBottom, TEXT="JAMES")
            toook_cathal = test_PiTFT.add_button(220, RowY, 300, RowBottom, TEXT="CATHAL")
            # Row2
            RowY = 90
            RowBottom = RowY + 60
            toook_diarmuid = test_PiTFT.add_button(10, RowY, 100, RowBottom, TEXT="Toomage")
            toook_James2 = test_PiTFT.add_button(110, RowY, 210, RowBottom, TEXT="McQuaid")
            toook_Barry = test_PiTFT.add_button(220, RowY, 300, RowBottom, TEXT="Barry")
            # Row3
            RowY = 160
            RowBottom = RowY + 60
            toook_Bag = test_PiTFT.add_button(10, RowY, 100, RowBottom, TEXT="New Bag")
            toook_Other = test_PiTFT.add_button(110, RowY, 210, RowBottom, TEXT="Others")
            toook_rolled = test_PiTFT.add_button(220, RowY, 300, RowBottom, TEXT="Rolled")

            while True:
                time.sleep(10)
        # Ctrl C
    except KeyboardInterrupt:
        print("User cancelled")
        raise

        # error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        if test_PiTFT:
            test_PiTFT.stop()